Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Astro
Upstream-Contact: Chris Phillips <Chris.Phillips@csiro.au>
Upstream-Name: Astro

Files: *
Copyright: 1999-2009, Chris Phillips <Chris.Phillips@csiro.au>
License: Artistic or GPL-1+
Comment: citing the README:
 .
 The J2000/B1950 routines are based on the FORTRAN version of
 SLALIB, which is under the GPL.
 .
 This comment refers to Astro/Coord.pm, which includes:
 .
 Copyright (C) 1995 Rutherford Appleton Laboratory
 Copyright (C) 1996 Rutherford Appleton Laboratory
 Copyright (C) 1998 Rutherford Appleton Laboratory
 .
 SLALIB is e.g. included in the iraf package in Debian main in the math/slalib
 directory, which contains a copy of the GPL-2 (named SLA_CONDITIONS).

Files: debian/*
Copyright: 2024, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
